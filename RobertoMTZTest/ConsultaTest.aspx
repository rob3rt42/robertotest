﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConsultaTest.aspx.cs" Inherits="RobertoMTZTest.ConsultaTest" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>
    <title>ConsultaEntidad</title>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" href="Recursos/styles/bootstrap.min.css" rel="stylesheet" />

    <!-- Custom stlylesheet -->
    <link type="text/css" href="Recursos/styles/style.css" rel="stylesheet" />
    <link href="Recursos/styles/test.css" rel="stylesheet" />




</head>

<body>
    <div id="booking" class="section">
        <div class="section-center">
            <div class="container">
                <div class="row">
                    <div class="booking-form">
                        <form runat="server" class="form">
                            <input type="hidden" id="positiontable" runat="server" />
                            <div class="col-md-6 titulo">
                                Sistema Prueba de Easy-Rez
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-3 col-md-offset-6">
                                    <button runat="server" type="button" onclick="CambioPersona('1')" class="button">Anterior</button>
                                </div>
                                <div class="col-md-3">
                                    <button class="button" type="button" onclick="CambioPersona('2')" runat="server">Siguiente </button>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="row justify-content-center rowp">
                                <div class="col-md-6">

                                    <label class="Titulos">Buscar por:</label>
                                    <select id="ListaEntidad1" onchange="CambioEntidad(this.value)">
                                        <option value='117'>Cliente</option>
                                        <option value='118'>Emisor</option>
                                        <option value='126'>Proveedor PAC</option>
                                    </select>
                                </div>
                                <div class="col-md-6">

                                    <label class="Titulos">Tipo:</label>
                                    <label class="labelRes" id="LabelTipoPersona" runat="server"></label>
                                </div>
                                <br />
                                <br />
                                <br />
                                <div class="col-md-4">

                                    <label class="Titulos">Razon social:</label><br />
                                    <label class="labelRes" id="LabelRazonSocial" runat="server"></label>
                                </div>
                                <div class="col-md-4">

                                    <label class="Titulos">RFC:</label><br />
                                    <label class="labelRes" id="LabelRFC" runat="server"></label>
                                </div>
                                <div class="col-md-4">

                                    <label class="Titulos">Correo:</label><br />
                                    <label class="labelRes" id="LabelCorreo" runat="server"></label>
                                </div>
                                <br />
                                <div class="col-md-4">

                                    <label class="Titulos">MetodoPago:</label><br />
                                    <label class="labelRes" id="LabelMetodoPago" runat="server"></label>
                                </div>
                                <div class="col-md-4">

                                    <label class="Titulos">UsoCFDI Clave:</label><br />
                                    <label class="labelRes" id="LabelUSOCFDICLAVE" runat="server"></label>
                                </div>
                                <div class="col-md-4">

                                    <label class="Titulos">UsoCFDI Descripcion:</label><br />
                                    <label class="labelRes" id="LabelUSOCFDIDES" runat="server"></label>
                                </div>
                                <div class="col-md-4">

                                    <label class="Titulos">TipoPersona:</label><br />
                                    <label class="labelRes" id="LabelTpersona1" runat="server"></label>
                                </div>
                                <div class="col-md-4">

                                    <label class="Titulos">RegimenFiscal Clave:</label><br />
                                    <label class="labelRes" id="LabelRegimen" runat="server"></label>
                                </div>
                                <div class="col-md-4">

                                    <label class="Titulos">RegimenFiscal Descripción:</label><br />
                                    <label class="labelRes" id="RegimenDesLabel" runat="server"></label>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<script src="Recursos/js/test.js"></script>
</html>
