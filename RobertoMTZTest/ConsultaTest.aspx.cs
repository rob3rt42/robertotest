﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;
using System.Web.Services;

namespace RobertoMTZTest
{
    public partial class ConsultaTest : System.Web.UI.Page
    {
        public int position = 0;
        public DataTable EntidadData = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            LlenarCamposBase("117");
        }
        //conexion a base
        public string ConexionBase()
        {
            string servidor = "localhost"; //Nombre o ip del servidor de MySQL
            string bd = "roberto"; //Nombre de la base de datos
            string usuario = "root"; //Usuario de acceso a MySQL
            string password = "RobertoMTZ"; //Contraseña de usuario de acceso a MySQL


            //Crearemos la cadena de conexión concatenando las variables
            string cadenaConexion = "Server=" + servidor + ";Database=" + bd + ";uid=" + usuario + "; pwd=" + password + "";
            return cadenaConexion;
        }
        public void LlenarCamposBase(string metodobusqueda)
        {
            //busqueda de informacion
            string BaseMysql = ConexionBase();

            using (MySqlConnection conexionsql = new MySqlConnection(BaseMysql))
            {
                conexionsql.Open();
                MySqlDataAdapter Adaptersql = new MySqlDataAdapter("BusquedaEntidadFinanciera", conexionsql);
                Adaptersql.SelectCommand.CommandType = CommandType.StoredProcedure;
                Adaptersql.SelectCommand.Parameters.AddWithValue("TipoEntidad", Convert.ToInt32(metodobusqueda));
                Adaptersql.Fill(EntidadData);
                Session["DataTable"] = EntidadData;
                llenadoinfoEntidad(0, EntidadData);
            }
        }
        public void llenadoinfoEntidad(int position, DataTable InfoDataTable)
        {
            //llenado de informacion
            LabelTipoPersona.InnerText = InfoDataTable.Rows[position]["TipoPersona"].ToString();
            LabelRazonSocial.InnerText = InfoDataTable.Rows[position]["RazonSocial"].ToString();
            LabelRFC.InnerText = InfoDataTable.Rows[position]["RFC"].ToString();
            LabelCorreo.InnerText = InfoDataTable.Rows[position]["correo"].ToString();
            LabelMetodoPago.InnerText = InfoDataTable.Rows[position]["MetodoPago"].ToString();
            LabelUSOCFDICLAVE.InnerText = InfoDataTable.Rows[position]["USOCFEDICLAVE"].ToString();
            LabelUSOCFDIDES.InnerText = InfoDataTable.Rows[position]["USOCFDIDES"].ToString();
            LabelTpersona1.InnerText = InfoDataTable.Rows[position]["TipoPersona"].ToString();
            LabelRegimen.InnerText = InfoDataTable.Rows[position]["RegimenClave"].ToString();
            RegimenDesLabel.InnerText = InfoDataTable.Rows[position]["RegimenDes"].ToString();
            positiontable.Value = position.ToString();
        }


        //Cambio de Entidad Tributaria
        [WebMethod]
        public static string[] CambioTipo(int position, string metodobusqueda, string tipobtn)
        {
            if (tipobtn == "1") { position = position - 1; } else { position = position + 1; }

            string servidor = "localhost"; //Nombre o ip del servidor de MySQL
            string bd = "roberto"; //Nombre de la base de datos
            string usuario = "root"; //Usuario de acceso a MySQL
            string password = "RobertoMTZ"; //Contraseña de usuario de acceso a MySQL


            //Crearemos la cadena de conexión concatenando las variables
            string cadenaConexion = "Server=" + servidor + ";Database=" + bd + ";uid=" + usuario + "; pwd=" + password + "";
            DataTable EntidadData = new DataTable();
            string[] respuesta = new string[11];

            using (MySqlConnection conexionsql = new MySqlConnection(cadenaConexion))
            {
                conexionsql.Open();
                MySqlDataAdapter Adaptersql = new MySqlDataAdapter("BusquedaEntidadFinanciera", conexionsql);
                Adaptersql.SelectCommand.CommandType = CommandType.StoredProcedure;
                Adaptersql.SelectCommand.Parameters.AddWithValue("TipoEntidad", Convert.ToInt32(metodobusqueda));
                Adaptersql.Fill(EntidadData);
            }
            if (position > EntidadData.Rows.Count - 1 && tipobtn == "2")
            {
                position = 0;
            }
            else if (position < 0 && tipobtn == "1")
            {
                position = 0;
            }
            ///Guardado de informacion en arreglo Ajax
            respuesta[0] = EntidadData.Rows[position]["TipoPersona"].ToString();
            respuesta[1] = EntidadData.Rows[position]["RazonSocial"].ToString();
            respuesta[2] = EntidadData.Rows[position]["RFC"].ToString();
            respuesta[3] = EntidadData.Rows[position]["correo"].ToString();
            respuesta[4] = EntidadData.Rows[position]["MetodoPago"].ToString();
            respuesta[5] = EntidadData.Rows[position]["USOCFEDICLAVE"].ToString();
            respuesta[6] = EntidadData.Rows[position]["USOCFDIDES"].ToString();
            respuesta[7] = EntidadData.Rows[position]["TipoPersona"].ToString();
            respuesta[8] = EntidadData.Rows[position]["RegimenClave"].ToString();
            respuesta[9] = EntidadData.Rows[position]["RegimenDes"].ToString();
            respuesta[10] = position.ToString();


            return respuesta;
        }
       
        /// Inicia cambio de entidad
        [WebMethod]
        public static string[] CambioEntidad(string tipoent)
        {


            string servidor = "localhost"; //Nombre o ip del servidor de MySQL
            string bd = "roberto"; //Nombre de la base de datos
            string usuario = "root"; //Usuario de acceso a MySQL
            string password = "RobertoMTZ"; //Contraseña de usuario de acceso a MySQL


            //Crearemos la cadena de conexión concatenando las variables
            string cadenaConexion = "Server=" + servidor + ";Database=" + bd + ";uid=" + usuario + "; pwd=" + password + "";
            DataTable EntidadData = new DataTable();
            string[] respuesta = new string[11];

            using (MySqlConnection conexionsql = new MySqlConnection(cadenaConexion))
            {
                conexionsql.Open();
                MySqlDataAdapter Adaptersql = new MySqlDataAdapter("BusquedaEntidadFinanciera", conexionsql);
                Adaptersql.SelectCommand.CommandType = CommandType.StoredProcedure;
                Adaptersql.SelectCommand.Parameters.AddWithValue("TipoEntidad", Convert.ToInt32(tipoent));
                Adaptersql.Fill(EntidadData);
            }
            ///Guardado de informacion en arreglo Ajax
            respuesta[0] = EntidadData.Rows[0]["TipoPersona"].ToString();
            respuesta[1] = EntidadData.Rows[0]["RazonSocial"].ToString();
            respuesta[2] = EntidadData.Rows[0]["RFC"].ToString();
            respuesta[3] = EntidadData.Rows[0]["correo"].ToString();
            respuesta[4] = EntidadData.Rows[0]["MetodoPago"].ToString();
            respuesta[5] = EntidadData.Rows[0]["USOCFEDICLAVE"].ToString();
            respuesta[6] = EntidadData.Rows[0]["USOCFDIDES"].ToString();
            respuesta[7] = EntidadData.Rows[0]["TipoPersona"].ToString();
            respuesta[8] = EntidadData.Rows[0]["RegimenClave"].ToString();
            respuesta[9] = EntidadData.Rows[0]["RegimenDes"].ToString();
            respuesta[10] = "0";


            return respuesta;
        }

    }
}