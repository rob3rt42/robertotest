﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InsertarEntidad.aspx.cs" Inherits="RobertoMTZTest.InsertarEntidad" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>
    <title>Insertar Entidad Tributaria</title>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" href="Recursos/styles/bootstrap.min.css" rel="stylesheet" />

    <!-- Custom stlylesheet -->
    <link type="text/css" href="Recursos/styles/style.css" rel="stylesheet" />
    <link href="Recursos/styles/test.css" rel="stylesheet" />




</head>

<body>
    <div id="booking" class="section">
        <div class="section-center">
            <div class="container">
                <div class="row">
                    <div class="booking-form">
                        <div class="col-md-6 titulo">
                            Sistema Prueba de Easy-Rez
                        </div>
                        <div class="col-md-6 titulo">
                            Insertar Entidad
                        </div>
                        <form runat="server" class="form">
                            <div class="row justify-content-center rowp">

                                <div class="col-md-4">

                                    <label class="labelRes">Razon Social:</label>
                                    <br />
                                    <asp:TextBox class="texboxin" ID="RazonTXT" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">

                                    <label class="labelRes">RFC:</label>
                                    <br />
                                    <asp:TextBox class="texboxin" ID="RFCTXT" runat="server"></asp:TextBox>
                                </div>

                                <div class="col-md-4">

                                    <label class="labelRes">Correo:</label>
                                    <br />
                                    <asp:TextBox ID="CorreoTXT" class="texboxin" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-12" style="margin-top: 30px;"></div>
                                <div class="col-md-6">

                                    <label class="labelRes">Tipo Persona:</label>
                                    <asp:DropDownList runat="server" class="texboxin" ID="TipoPersonalista">
                                        <asp:ListItem Value='select'>Tipo Persona</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-6">

                                    <label class="labelRes">Es sucursal:</label>
                                    <asp:RadioButton runat="server" class="texboxin" ID="radiosucursal"></asp:RadioButton>
                                </div>
                                <div class="col-md-12" style="margin-top: 30px;"></div>
                                <div class="col-md-12">

                                    <label class="labelRes">Tipo Entidad:</label>
                                    <asp:DropDownList class="texboxin" runat="server" ID="ListaEntidad">
                                        <asp:ListItem Value='select'>Tipo Entitad</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <br />
                                <br />
                                <div class="col-md-12" style="margin-top: 30px;"></div>
                                <div class="col-md-12">

                                    <label class="labelRes">Tipo Regimen:</label>
                                    <asp:DropDownList class="texboxin" runat="server" ID="RegimenList">
                                        <asp:ListItem Value='select'>Tipo Regimen</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <br />
                                <br />
                                <div class="col-md-12" style="margin-top: 30px;"></div>
                                <div class="col-md-12">

                                    <label class="labelRes">Tipo USOCFDI:</label>
                                    <asp:DropDownList runat="server" class="texboxin" ID="UsoCFDIList">
                                        <asp:ListItem Value='select'>Tipo USOCFDI</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <br />
                                <br />
                                <div class="col-md-12" style="margin-top: 20px;"></div>
                                <div class="col-md-6">

                                    <button class="button">Cancelar</button>
                                </div>
                                <div class="col-md-6">
                                    <asp:Button runat="server" class="button" OnClick="Guardar" Text="Continuar"></asp:Button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<script src="Recursos/js/test.js"></script>
</html>
