﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RobertoMTZTest
{
    public partial class InsertarEntidad : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //llenado Info
            BuscarCFDI();
            BuscarRegimen();
            BuscarEntidad();
            BuscarTPersona();
        }
        //Insertar Entidad
        protected void Guardar(object sender, EventArgs e)

        {
            
            string essucursal = "No";
            string BaseMysql = ConexionBase();
            ; if (radiosucursal.Checked) { essucursal = "Si"; }
            //Inicio de creacion entidad 
            using (MySqlConnection conexionsql = new MySqlConnection(BaseMysql))
            {
                //Abriendo Conexion
                conexionsql.Open();
                MySqlCommand sqlcommand = new MySqlCommand("CrearEntidadTributaria", conexionsql);
                sqlcommand.CommandType = CommandType.StoredProcedure;
                sqlcommand.Parameters.AddWithValue("Correo", CorreoTXT.Text);
                sqlcommand.Parameters.AddWithValue("tipopersona", Convert.ToInt32(TipoPersonalista.SelectedValue));
                sqlcommand.Parameters.AddWithValue("rfc", RFCTXT.Text);
                sqlcommand.Parameters.AddWithValue("razonsocial", RazonTXT.Text);
                sqlcommand.Parameters.AddWithValue("metodopago", 6);
                sqlcommand.Parameters.AddWithValue("uocfdi", Convert.ToInt32(UsoCFDIList.SelectedValue));
                sqlcommand.Parameters.AddWithValue("regimen", Convert.ToInt32(RegimenList.SelectedValue));
                sqlcommand.Parameters.AddWithValue("sucursal", essucursal);
                sqlcommand.Parameters.AddWithValue("entidadt", Convert.ToInt32(ListaEntidad.SelectedValue));
                sqlcommand.ExecuteNonQuery();
                //Creacion entidad
                Response.Write("<script>alert('Registro completado')</script>");
            }
        }
        //crear conexion base
        public string ConexionBase()
        {
            string servidor = "localhost"; //Nombre o ip del servidor de MySQL
            string bd = "roberto"; //Nombre de la base de datos
            string usuario = "root"; //Usuario de acceso a MySQL
            string password = "RobertoMTZ"; //Contraseña de usuario de acceso a MySQL


            //Crearemos la cadena de conexión concatenando las variables
            string cadenaConexion = "Server=" + servidor + ";Database=" + bd + ";uid=" + usuario + "; pwd=" + password + "";
            return cadenaConexion;
        }
         //busqueda de datos cfdi
         public void BuscarCFDI()
        {
            //Inicio Funcion Busqueda
            string BaseMysql = ConexionBase();
            DataTable resultadoBase = new DataTable();
            using (MySqlConnection conexionSQL = new MySqlConnection(BaseMysql))
            {
                //Abrir  conexin 
                conexionSQL.Open();
                MySqlDataAdapter AdapterSQL = new MySqlDataAdapter("cfdibusqueda", conexionSQL);
                AdapterSQL.SelectCommand.CommandType = CommandType.StoredProcedure;

                AdapterSQL.Fill(resultadoBase);
                for (int i = 0; i <= resultadoBase.Rows.Count - 1; i++)
                {
                    UsoCFDIList.Items.Add(new ListItem(resultadoBase.Rows[i]["Clave"].ToString() + " - " + resultadoBase.Rows[i]["Descripcion"].ToString(), resultadoBase.Rows[i]["ID"].ToString()));
                }

            }
        }
        //busqueda de datos Regimen
        public void BuscarRegimen()
        {
            //Inicio Funcion Busqueda
            string BaseMysql = ConexionBase();
            DataTable resultadoBase = new DataTable();
            using (MySqlConnection conexionSQL = new MySqlConnection(BaseMysql))
            {
                //Abrir  conexin 
                conexionSQL.Open();
                MySqlDataAdapter AdapterSQL = new MySqlDataAdapter("buscarregimen", conexionSQL);
                AdapterSQL.SelectCommand.CommandType = CommandType.StoredProcedure;

                AdapterSQL.Fill(resultadoBase);
                for (int i = 0; i <= resultadoBase.Rows.Count - 1; i++)
                {
                    RegimenList.Items.Add(new ListItem(resultadoBase.Rows[i]["Clave"].ToString() + " - " + resultadoBase.Rows[i]["Descripcion"].ToString(), resultadoBase.Rows[i]["ID"].ToString()));
                }

            }
        }
        //busqueda de datos Regimen
        public void BuscarEntidad()
        {
            //Inicio Funcion Busqueda
            string BaseMysql = ConexionBase();
            DataTable resultadoBase = new DataTable();
            using (MySqlConnection conexionSQL = new MySqlConnection(BaseMysql))
            {
                //Abrir  conexin 
                conexionSQL.Open();
                MySqlDataAdapter AdapterSQL = new MySqlDataAdapter("busquedaentidad", conexionSQL);
                AdapterSQL.SelectCommand.CommandType = CommandType.StoredProcedure;

                AdapterSQL.Fill(resultadoBase);
                for (int i = 0; i <= resultadoBase.Rows.Count - 1; i++)
                {
                    ListaEntidad.Items.Add(new ListItem(resultadoBase.Rows[i]["Nombre"].ToString(), resultadoBase.Rows[i]["ID"].ToString()));
                }

            }
        }
        //busqueda de datos Regimen
        public void BuscarTPersona()
        {
            //Inicio Funcion Busqueda
            string BaseMysql = ConexionBase();
            DataTable resultadoBase = new DataTable();
            using (MySqlConnection conexionSQL = new MySqlConnection(BaseMysql))
            {
                //Abrir  conexin 
                conexionSQL.Open();
                MySqlDataAdapter AdapterSQL = new MySqlDataAdapter("buscartipopersona", conexionSQL);
                AdapterSQL.SelectCommand.CommandType = CommandType.StoredProcedure;

                AdapterSQL.Fill(resultadoBase);
                for (int i = 0; i <= resultadoBase.Rows.Count - 1; i++)
                {
                    TipoPersonalista.Items.Add(new ListItem(resultadoBase.Rows[i]["Nombre"].ToString(), resultadoBase.Rows[i]["ID"].ToString()));
                }

            }
        }
    }
}