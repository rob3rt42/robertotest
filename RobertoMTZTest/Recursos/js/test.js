﻿//Cambio de Entidad
function CambioPersona(Tipo) {
    let positionData = document.getElementById("positiontable").value; 
    let tentidadt = document.getElementById("ListaEntidad1").value;
    $.ajax({
        type: "POST", // Tipo de llamada
        url: "../ConsultaTest.aspx/CambioTipo",   //Dirección del WebMethod, o sea, Página.aspx/Método *NOTA: La función debe estar declarada como <WebMethod>
        data: "{position: " + positionData + ",metodobusqueda:'" + tentidadt + "',tipobtn:" + Tipo +"}",
        contentType: "application/json; charset=utf-8",                                                                      //Tipo de contenido
        dataType: "json",                                                                                                    //Tipo de datos
        success: asegpintar, //Función a la cual llamar cuando se pudo llamar satisfactoriamente al método                 
        //Función a la cual llamar cuando se producen errores
    });
    function asegpintar(msg) {
        //LLenado de informacion
        document.getElementById("LabelTipoPersona").innerText = msg.d[0];
        document.getElementById("LabelRazonSocial").innerText = msg.d[1];
        document.getElementById("LabelRFC").innerText = msg.d[2];
        document.getElementById("LabelCorreo").innerText = msg.d[3];
        document.getElementById("LabelMetodoPago").innerText = msg.d[4];
        document.getElementById("LabelUSOCFDICLAVE").innerText = msg.d[5];
        document.getElementById("LabelUSOCFDIDES").innerText = msg.d[6];
        document.getElementById("LabelTpersona1").innerText = msg.d[7];
        document.getElementById("LabelRegimen").innerText = msg.d[8];
        document.getElementById("RegimenDesLabel").innerText = msg.d[9];
        document.getElementById("positiontable").value = msg.d[10];
    }

  
}
//Cambio de tipo de entidad tributaria seleccionada
function CambioEntidad(Tipo) {
    $.ajax({
        type: "POST", // Tipo de llamada
        url: "../ConsultaTest.aspx/CambioEntidad",                                                      //Dirección del WebMethod, o sea, Página.aspx/Método *NOTA: La función debe estar declarada como <WebMethod>
        data: "{tipoent:" + Tipo + "}",
        contentType: "application/json; charset=utf-8",                                                                      //Tipo de contenido
        dataType: "json",                                                                                                    //Tipo de datos
        success: asegpintar, //Función a la cual llamar cuando se pudo llamar satisfactoriamente al método                 
        //Función a la cual llamar cuando se producen errores
    });
    function asegpintar(msg) {
        //llenado de informacion
        document.getElementById("LabelTipoPersona").innerText = msg.d[0];
        document.getElementById("LabelRazonSocial").innerText = msg.d[1];
        document.getElementById("LabelRFC").innerText = msg.d[2];
        document.getElementById("LabelCorreo").innerText = msg.d[3];
        document.getElementById("LabelMetodoPago").innerText = msg.d[4];
        document.getElementById("LabelUSOCFDICLAVE").innerText = msg.d[5];
        document.getElementById("LabelUSOCFDIDES").innerText = msg.d[6];
        document.getElementById("LabelTpersona1").innerText = msg.d[7];
        document.getElementById("LabelRegimen").innerText = msg.d[8];
        document.getElementById("RegimenDesLabel").innerText = msg.d[9];
        document.getElementById("positiontable").value = msg.d[10];
    }


}